package com.array.problems;

public class EvenElementsFirst {
    String Problem = "Even elements in array should come first and odd elements at last ";

    public static void main(String[] args) {
        int[] input = new int[]{12,43,13,51,124,613,657,858,674,325,451,84,36,21};
        int[] output = new int[]{12,124,858,674,84,36,43,13,51,613,657,325,451,21};
        System.out.println(""+input.length);
        System.out.println(""+output.length);

    }
}
