package com.array.problems;

import com.sort.QuickSort;
import com.sun.swing.internal.plaf.synth.resources.synth_sv;

import java.util.Arrays;
import java.util.function.Consumer;

/**
 * concentrate on constant things what are constant things 0,1,2
 * */
public class DutchFlagProblem {
    String Problem = "For this problem, your goal is to sort an array of 0, 1 and 2's but you must do this in place," +
            " in linear time and without any extra space (such as creating an extra array). " +
            "This is called the Dutch national flag sorting problem. " +
            "For example, if the input array is [2,0,0,1,2,1] then your program should output " +
            "[0,0,1,1,2,2] and the algorithm should run in O(n) time.";

    public static void main(String[] args) {
        int[] input = new int[]{2, 0, 0, 1, 2, 1};
        int[] output = new int[]{0, 0, 1, 1, 2, 2};
        System.out.println(input.equals(linearTimeSolution(input)));
      /*  threeWayPartition(input,input.length-1);
        Arrays.stream(input).forEach(t-> System.out.println(t));*/
    }

    public static int[] linearTimeSolution(int[] input){
        int pivot =  1;
        int start = 0,mid = 0;
        int end =  input.length -1;
        while(mid <=end){
            if(input[mid]  < pivot){ // 0 cases
                swap(input,start,mid);
                start++;
                mid++;
            }
            if(input[mid] > pivot){ // 2 cases
                end--;
            }

            if(input[start]  == pivot){ // 1 cases
                mid++;
            }



        }
        return input;
    }

    public static int[] simplestSolution(int[] input) {
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input.length; j++) {
                if (input[j] > input[i]) {
                    int temp = input[i];
                    input[i] = input[j];
                    input[j] = temp;
                }

            }
        }
        return input;
    }

    public static void threeWayPartition(int[] A, int end)
    {
        int start = 0, mid = 0;
        int pivot = 1;

        while (mid <= end)
        {
            if (A[mid] < pivot)         // current element is 0
            {
                swap(A, start, mid);
                ++start;
                ++mid;
            }
            else if (A[mid] > pivot)    // current element is 2
            {
                swap(A, mid, end);
                --end;
            }
            else                        // current element is 1
                ++mid;
        }
    }

    // Utility function to swap two elements A[i] and A[j] in the array
    private static void swap(int[] A, int i, int j) {
        int temp = A[i];
        A[i] = A[j];
        A[j] = temp;
    }

    public static int[] linearSolution(int[] input) {
        int pivot  =  3;
        int pivotElement =  1;
        int first =  0;
        int last =  input.length -1;
        while (first<= last){
            if(input[first]> pivotElement){
                // swap
                // increment

            }
        }
        return  new int[]{1};
    }


        // through quick sort // basic and simple
    public static int[] sortingSolution(int[] input) {
        new QuickSort().quickSort(input,0,input.length-1);
     return input;
    }
}
