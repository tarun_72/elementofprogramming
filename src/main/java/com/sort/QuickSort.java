package com.sort;

public class QuickSort {

    public void quickSort(int[] array, int first, int last) {
        int partitionElement = partition(array, first, last);
        if (first < partitionElement-1) {
            quickSort(array, first, partitionElement-1);
        }
        if (partitionElement < last) {
            quickSort(array, partitionElement , last);
        }
    }

    private int partition(int[] array, int first, int last) {
        int pivot = (first + last) / 2;
        int start = first;
        int end = last;
        int pivotElement = array[pivot];
        while (start <= end) {
            while (array[start] <pivotElement ) {
                start++;
            }
            while (array[end] > pivotElement) {
                end--;
            }
            if (start <= last) {

                int temp = array[start];
                array[start] = array[end];
                array[end] = temp;
                start++;
                end--;
            }
        }
        return start;
    }
}
